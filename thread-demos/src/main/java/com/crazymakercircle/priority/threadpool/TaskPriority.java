package com.crazymakercircle.priority.threadpool;

import lombok.Data;

/**
 * 任务优先级枚举：HIGH > MEDIUM > LOW
 */

public enum TaskPriority {
    HIGH(1),
    MEDIUM(2),
    LOW(3);

    private Integer value;

    TaskPriority(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}

