package com.crazymakercircle.priority.threadpool;

import com.crazymakercircle.util.Print;
import lombok.Data;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 优先级任务
 */
@Data
public class PriorityTask implements Runnable {
    private static final AtomicInteger SEQUENCE_GENERATOR = new AtomicInteger(1);
    private final Callable<String> inner;
    private final int priority;
    private final int taskSequence;
    public PriorityTask(Callable<String> inner, Integer priority) {
        this.inner = inner;
        this.priority = priority;
        this.taskSequence = SEQUENCE_GENERATOR.getAndIncrement();
    }

    @Override
    public void run() {
        try {
            String r = inner.call();
            Print.tcfo("priority " + priority + " is called. result is:" + r);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

