package com.crazymakercircle.httpclient;

public interface HttpResponse<T> {

    int status();

    String header(String header);

    String header(String header, String defaultVal);

    T content();

    Object err();

}
